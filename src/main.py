import random
import time

import pandas as pd

from tanning_bot import Hide
from tanning_bot import TanBot
from utilities.bot import Bot
from utilities.chat.read_chat import read_chat
from utilities.chat.utils import every
from utilities.take_break import TakeBreak


bot = Bot()


# breaker.login()
# python src/utilities/server.py
# pip install simplejson numpy pywin32 pyautogui opencv-python pillow
def run_tan_bot():
    hide = Hide.COW_HIDE.value
    tanned_hide = Hide.HARD_LEATHER.value

    breaker = TakeBreak()
    tan_bot = TanBot(hide, tanned_hide)

    time_start = time.time()
    time_duration_min = random.randint(120, 180)

    while True:
        # Run script for 2 to 3 hours.
        while time.time() < time_start + time_duration_min * 60:
            tan_bot.run()
        tan_bot.inv_open = False
        # Take a break of 5 to 15 minutes.
        breaker.take_break()
        time_start = time.time()


def track_chat():
    every(delay_seconds=1, task=bot.read_chat)


if __name__ == "__main__":

    # run_tan_bot()
    track_chat()
