import platform
from pathlib import Path

import numpy as np
import PIL
import pyscreeze


# type error fix: https://stackoverflow.com/a/76525123
__PIL_TUPLE_VERSION = tuple(int(x) for x in PIL.__version__.split("."))
pyscreeze.PIL__version__ = __PIL_TUPLE_VERSION


RUNELITE_APP_NAME: str = "RuneLite"

OS = platform.system()

OS_TOP_BORDER: int = 0

if OS == "Darwin":
    OS_TOP_BORDER = 25
elif OS == "Windows":
    OS_TOP_BORDER = 0
elif OS == "Linux":
    OS_TOP_BORDER = 25
else:
    raise ValueError("Sorry, this OS is not supported at this point!")


if OS == "Linux":
    import os

    os.environ["DISPLAY"] = ":10.0"

# offsets
CLIENT_TOP_BORDER: int = 30
CLIENT_SIDE_BORDER: int = 35

OFFSET_MINIMAP_X: int = 160 - CLIENT_SIDE_BORDER
OFFSET_MINIMAP_Y: int = 137 - CLIENT_TOP_BORDER - OS_TOP_BORDER

OFFSET_RUN_X: int = 276 - CLIENT_SIDE_BORDER
OFFSET_RUN_Y: int = 249 - CLIENT_TOP_BORDER - OS_TOP_BORDER

OFFSET_LOGOUT_X: int = 56 - CLIENT_SIDE_BORDER
OFFSET_LOGOUT_Y: int = 74 - CLIENT_TOP_BORDER - OS_TOP_BORDER

CHAT_WINDOW_X: int = 0
CHAT_WINDOW_Y: int = 340
CHAT_WINDOW_WIDTH: int = 516
CHAT_WINDOW_HEIGHT: int = 120

TILES_PIXELS: int = 8


HUE_BLUE_LOWER_BOUND: int = 105
HUE_BLUE_UPPER_BOUND: int = 135

# CHAT
PUBLIC_CHAT_RGB: list[int] = [0, 0, 255]
PUBLIC_CHAT_HSV_LOWER_BOUNDS: np.array = np.array([HUE_BLUE_LOWER_BOUND, 0, 20])
PUBLIC_CHAT_HSV_UPPER_BOUNDS: np.array = np.array([HUE_BLUE_UPPER_BOUND, 255, 255])

# file structure
ROOT_DIR = Path(__file__).parent
CHAT_SCREENSHOT_DIR = ROOT_DIR / "utilities" / "chat" / "screenshots"
