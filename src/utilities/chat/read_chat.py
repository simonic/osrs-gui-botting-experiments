import glob
import os
from datetime import datetime

import cv2
import pandas as pd
import pyscreenshot
import pytesseract

from src.consts import CHAT_SCREENSHOT_DIR
from src.consts import PUBLIC_CHAT_HSV_LOWER_BOUNDS
from src.consts import PUBLIC_CHAT_HSV_UPPER_BOUNDS
from utilities.window_capture import CapturedWindow
from utilities.window_capture import captured_window


# TODO: make OS-independent
pytesseract.pytesseract.tesseract_cmd = r"/opt/homebrew/bin/tesseract"


window: CapturedWindow = captured_window()


def interpret_chat_screenshot(file_path) -> pd.DataFrame:
    image = cv2.imread(str(file_path))

    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    # cv2.imshow("HSV Image", image_hsv)

    # Defining mask for detecting color
    mask = cv2.inRange(image_hsv, PUBLIC_CHAT_HSV_LOWER_BOUNDS, PUBLIC_CHAT_HSV_UPPER_BOUNDS)

    invert = 255 - mask

    # Display Image and Mask
    # cv2.imshow("Image", image)
    # cv2.imshow("Mask", mask)
    # cv2.imshow("inverted mask", invert)

    # Perform text extraction
    data: str = pytesseract.image_to_string(invert, config="--psm 6")

    # split lines and filter out empty strings
    lines: list[str] = [line for line in data.split("\n") if line]

    lines_df: pd.DataFrame = pd.DataFrame({"lines": lines})

    # Make python sleep for unlimited time
    # cv2.waitKey(0)
    return lines_df


def take_chat_screenshot(file_format="png"):
    unix_timestamp: int = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
    file_path: str = f"{CHAT_SCREENSHOT_DIR}/{unix_timestamp}.{file_format}"

    x, y, width, height = window.get_chat_window()
    chat_screenshot = pyscreenshot.grab(bbox=(x, y, x + width, y + height))

    chat_screenshot.save(file_path)


def get_latest_screenshot():
    all_screenshots = CHAT_SCREENSHOT_DIR.glob("*.png")

    latest_screenshot = max(all_screenshots, key=os.path.getctime)

    return latest_screenshot


def cleanup_screenshots():
    """Clears screenshots directory except for latest screenshot."""
    all_screenshots = glob.glob(f"{CHAT_SCREENSHOT_DIR}/*.png")
    latest_screenshot_idx = max((v, i) for i, v in enumerate(all_screenshots))[1]
    all_screenshots.pop(latest_screenshot_idx)

    for screenshot in all_screenshots:
        os.remove(screenshot)


def read_chat():
    take_chat_screenshot()
    latest_screenshot = get_latest_screenshot()
    new_lines = interpret_chat_screenshot(file_path=latest_screenshot)
    cleanup_screenshots()

    return new_lines


if __name__ == "__main__":
    new_lines = read_chat()
    from pprint import pprint

    print()
    print()
    pprint(new_lines)
    print()
    print()
