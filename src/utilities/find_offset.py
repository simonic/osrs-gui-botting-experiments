import pyautogui

from src.consts import CLIENT_SIDE_BORDER
from src.consts import CLIENT_TOP_BORDER
from src.consts import OFFSET_MINIMAP_X
from src.consts import OFFSET_MINIMAP_Y
from src.consts import OS
from src.consts import OS_TOP_BORDER
from src.consts import RUNELITE_APP_NAME
from src.utilities.window_capture import CapturedWindow
from src.utilities.window_capture import captured_window


# ONLY USED TO CONFIGURE OFFSETS, NOT TO RUN SCRIPT.

window: CapturedWindow = captured_window()

from pprint import pprint


print()
print()
pprint(window)
print()
print()

x, y = window.center_minimap

print(OFFSET_MINIMAP_X + CLIENT_SIDE_BORDER, OFFSET_MINIMAP_Y + CLIENT_TOP_BORDER + OS_TOP_BORDER)
print(x, y)
# pyautogui.displayMousePosition()
# pyautogui.moveTo(x=x, y=y, duration=0.5)
pyautogui.click(x=x, y=y, duration=0.5)
