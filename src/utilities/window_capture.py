from typing import Any

from src.consts import CHAT_WINDOW_HEIGHT
from src.consts import CHAT_WINDOW_WIDTH
from src.consts import CHAT_WINDOW_X
from src.consts import CHAT_WINDOW_Y
from src.consts import CLIENT_SIDE_BORDER
from src.consts import CLIENT_TOP_BORDER
from src.consts import OFFSET_LOGOUT_X
from src.consts import OFFSET_LOGOUT_Y
from src.consts import OFFSET_MINIMAP_X
from src.consts import OFFSET_MINIMAP_Y
from src.consts import OFFSET_RUN_X
from src.consts import OFFSET_RUN_Y
from src.consts import OS
from src.consts import RUNELITE_APP_NAME


class CapturedWindow:
    x = None
    y = None
    width = None
    height = None

    def __init__(self):
        self.title = RUNELITE_APP_NAME

        self.window = self.get_window(window_name=RUNELITE_APP_NAME)
        self.center_screen = self.get_window_center()
        self.center_minimap = self.get_center_minimap()
        self.run_button = self.get_run_button()
        self.logout_cross = self.get_logout_cross()

    def get_window(self, window_name: str) -> list[int]:
        """Returns the position of the window and the size of the window excluding the borders."""
        ...

    def get_center_minimap(self) -> list[float]:
        """Returns the coordinates of the center of the minimap."""
        map_center_x = self.x + self.width - OFFSET_MINIMAP_X
        map_center_y = self.y + OFFSET_MINIMAP_Y
        return [map_center_x, map_center_y]

    def get_window_center(self) -> list[int]:
        """Returns the center of the window, excluding the borders."""
        center_x = round(self.x + self.width / 2)
        center_y = round(self.y + self.height / 2)
        return [center_x, center_y]

    def get_run_button(self) -> list[int]:
        run_x = self.x + self.width - OFFSET_RUN_X
        run_y = self.y + OFFSET_RUN_Y
        return [run_x, run_y]

    def get_logout_cross(self) -> list[int]:
        run_x = self.x + self.width - OFFSET_LOGOUT_X
        run_y = self.y + OFFSET_LOGOUT_Y
        return [run_x, run_y]

    def get_chat_window(self) -> list[int]:
        chat_window_x = self.x + CHAT_WINDOW_X
        chat_window_y = self.y + CHAT_WINDOW_Y
        return [chat_window_x, chat_window_y, CHAT_WINDOW_WIDTH, CHAT_WINDOW_HEIGHT]

    def __repr__(self):
        return f"Title: {self.title} | X: {self.x} | Y: {self.y} | width: {self.width} | height: {self.height}"


class CapturedWindowWindows(CapturedWindow):
    def get_window(self, window_name: str) -> None:
        """Returns the position of the window and the size of the window excluding the borders."""
        import win32gui

        # Get window handle.
        hwnd = win32gui.FindWindow(None, window_name)
        # Set window to foreground.
        win32gui.SetForegroundWindow(hwnd)
        # Get the window size.
        rect = win32gui.GetWindowRect(hwnd)
        # Adjust size for borders
        self.x = rect[0]
        self.y = rect[1] + CLIENT_TOP_BORDER
        self.width = rect[2] - self.x - CLIENT_SIDE_BORDER
        self.height = rect[3] - self.y - CLIENT_TOP_BORDER


class CapturedWindowMacOS(CapturedWindow):
    def get_window(self, window_name: str) -> None:
        from Quartz.CoreGraphics import CGWindowListCopyWindowInfo
        from Quartz.CoreGraphics import CGWindowListOption
        from Quartz.CoreGraphics import kCGNullWindowID

        on_screen_only: CGWindowListOption = CGWindowListOption(1)

        open_windows_info: tuple[dict[str, Any]] = CGWindowListCopyWindowInfo(
            on_screen_only, kCGNullWindowID
        )

        runelite_window_info: dict = [
            window_info
            for window_info in open_windows_info
            if window_info["kCGWindowOwnerName"] == RUNELITE_APP_NAME
            and window_info["kCGWindowLayer"] == 0
        ][0]

        window_bounds: dict[str, int | str] = runelite_window_info["kCGWindowBounds"]

        self.x = int(window_bounds["X"])
        self.y = int(window_bounds["Y"]) + CLIENT_TOP_BORDER
        self.width = window_bounds["Width"] - CLIENT_SIDE_BORDER
        self.height = window_bounds["Height"] - CLIENT_TOP_BORDER


class CapturedWindowLinux(CapturedWindow):
    def get_window(self, window_name: str) -> list[int]:
        from gi.repository import Gtk
        from gi.repository import Wnck

        Gtk.init([])  # necessary only if not using a Gtk.main() loop
        screen = Wnck.Screen.get_default()
        screen.force_update()  # recommended per Wnck documentation

        window: Wnck.Window = [w for w in screen.get_windows() if w.get_name() == window_name][0]
        self.x, self.y, self.width, self.height = window.get_geometry()

        # clean up Wnck (saves resources, check documentation)
        window = None
        screen = None
        Wnck.shutdown()


def captured_window() -> CapturedWindowMacOS | CapturedWindowWindows | CapturedWindowLinux:
    """
    Factory function for returning the appropriate CapturedWindow class.
    :return:
    """

    if OS == "Darwin":
        return CapturedWindowMacOS()
    elif OS == "Windows":
        return CapturedWindowWindows()
    elif OS == "Linux":
        return CapturedWindowLinux()
    else:
        raise ValueError("Sorry, this OS is not supported at this point!")
