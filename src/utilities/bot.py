import pandas as pd

from utilities.chat.read_chat import read_chat
from utilities.window_capture import CapturedWindow
from utilities.window_capture import captured_window


class Bot:
    def __init__(self):
        self.window: CapturedWindow = captured_window()
        self.chat_history: pd.DataFrame = pd.DataFrame({"lines": []})

    def read_chat(self):
        new_lines = read_chat()

        # merge new lines into chat history, taking care of overlap
        self.chat_history = (
            self.chat_history.set_index("lines")
            .combine_first(new_lines.set_index("lines"))
            .reset_index()
        )
